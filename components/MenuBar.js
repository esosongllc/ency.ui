

const MenuBar = ({ name, controls }) => (
    <div className="encyui-main-menubar">
        <div className="encyui-main-ui-gutter"></div>
        <div className="encyui-center-view">
            <div className="menubar-left menubar-name">
                {name}
            </div>

            <div className="menubar-right">
               { controls }
            </div>
        </div>
        <div className="encyui-main-ui-gutter"></div>
    </div>
)

export default MenuBar