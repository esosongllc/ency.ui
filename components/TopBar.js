import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import EncyMenu from '../controls/menu'

const TopBar = ({ breadcrumb }) => (
    <div className="encyui-main-topbar">

        <div className="topbar-left">
            <span>

            </span>
            <span>
                {breadcrumb}
            </span>

            <span className="main-plus">
                <a><i className="fa fa-plus-circle fa-2x" aria-hidden="true"></i></a>
            </span>
        </div>

        <div className="topbar-middle">
            <span></span>
            <input type="search" autoComplete="off" placeholder="Search" />
        </div>

        <div className="topbar-right">
            <span>User</span>
            <span>User Profile</span>
        </div>
    </div>
)


export default TopBar