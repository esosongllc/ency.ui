import React from 'react'
import { Field, reduxForm } from 'redux-form'
import axios from 'axios'
import moment from 'moment'

import EditPage from '../../../controls/EditPage'
import { Section } from '../../../controls/layout'
import { TextBox, InputField, DateField, ComboField, RequiredValidator } from '../../../controls/input'

const titles = [
    { name: 'Mr.' },
    { name: 'Mrs.' },
    { name: 'Ms.' },
    { name: 'Miss.' },
    { name: 'Dr.' },
    { name: 'Prof.' },
    { name: 'Madam.' },
    { name: 'Master.' },
    { name: 'Ph.D.' },
]

const genders = [
    { name: 'Male' },
    { name: 'Female' },
    { name: 'Others' }
]

const nationality = [
    { name: 'Ghanaian' },
    { name: 'Togolese' },
    { name: 'Others' }
]

class BasicInfo extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { employee, onClose } = this.props
        const birthDate = moment(employee.birthDate).format('DD-MMM-YYYY') 

        return (
            <EditPage form="BasicInfo" onClose={onClose} >

                <form>
                    <Section>
                        <div className="col50 padd-right-15">
                            <Field type="text"
                                name="title"
                                label="Title"
                                component={ComboField}
                                validate={RequiredValidator}
                                dataSource={titles}
                            />

                            <Field type="text"
                                name="firstName"
                                label="First Name"
                                component={InputField}
                                validate={RequiredValidator}
                            />

                            <Field type="text"
                                name="lastName"
                                label="Last Name"
                                component={InputField}
                                validate={RequiredValidator}
                            />
                        </div>

                        <div className="col50 padd-left-15">

                            <Field type="text"
                                name="middleName"
                                label="Middle Name"
                                component={InputField}
                            />

                            <Field type="date"
                                name="birthDate"
                                label="Date of Birth"
                                defaultValue={birthDate}
                                component={DateField}
                            />

                            <Field type="text"
                                name="gender"
                                label="Gender"
                                component={ComboField}
                                validate={RequiredValidator}
                                dataSource={genders}
                            />

                            <Field type="text"
                                name="nationality"
                                label="Nationality"
                                component={ComboField}
                                dataSource={nationality}
                            />
                        </div>
                    </Section>
                </form>
            </EditPage>
        )
    }
}


export default reduxForm({ form: 'BasicInfo' })(BasicInfo)

