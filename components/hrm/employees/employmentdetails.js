import React from 'react'
import EditPage from '../../../controls/EditPage'


class EmploymentDetails extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { onClose } = this.props
        
        return (
            <EditPage onClose={onClose}>
                Employee Details
            </EditPage>
        )
    }
}

export default EmploymentDetails

