import React from 'react'
import { Section } from '../../../controls/layout'
import { HeadingLinks } from '../../../controls/HeadingLinks'
import { NonEditableTextField, NonEditableTextArea } from '../../../controls/input'

class DepartmentView extends React.Component {

    render() {
        const { department } = this.props


        return (
            <div>
                <Section>

                </Section>

                <Section>
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Department" value={department.name} bold={true}/>
                        <NonEditableTextArea label="Description" value={department.description}  />
                    </div>

                    <div className="col50 padd-left-15">
                        <NonEditableTextField label="Leave Block" value={department.leaveBlock} />
                    </div>
                </Section>
            </div>
        )
    }
}

export default DepartmentView