import React from 'react'
import { Section } from '../../../controls/layout'
import { HeadingLinks } from '../../../controls/HeadingLinks'
import { NonEditableTextField, NonEditableTextArea } from '../../../controls/input'

class EmploymentTypeView extends React.Component {

    render() {
        const { employmentType } = this.props


        return (
            <div>
                <Section>

                </Section>

                <Section>
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Employment Type" value={employmentType.name} bold={true}/>
                    </div>

                    <div className="col50 padd-left-15">
                        <NonEditableTextArea label="Description" value={employmentType.description}  />
                    </div>
                </Section>
            </div>
        )
    }
}

export default EmploymentTypeView