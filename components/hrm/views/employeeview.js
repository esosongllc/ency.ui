import React from 'react'
import {Section} from '../../../controls/layout'
import { HeadingLinks } from '../../../controls/HeadingLinks'
import {NonEditableTextField} from '../../../controls/input'
import moment from 'moment'


const formatDate = (date) =>moment(date).format('DD-MMM-YYYY')

class EmployeeView extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { employee } = this.props

        const middleName = employee.middleName ? `${employee.middleName} ` : ''
        const fullName = `${employee.firstName} ${middleName}${employee.lastName}`
        const birthDate = moment(employee.birthDate).format('DD-MMM-YYYY')

        return (
            <div>
                <Section>
                    <HeadingLinks label="Leave and Attendance" links={[
                        { url: '/employees', label: 'Attendance' },
                        { url: '/attendance', label: 'Leave Application' },
                        { url: '/employees', label: 'Leave Allocated' },
                        { url: '/attendance', label: 'Appraisal' },
                    ]} />
                </Section>

                <Section >
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Title" value={employee.title} />
                        <NonEditableTextField label="Full Name" value={fullName} bold={true} />
                        <NonEditableTextField label="User Id" value={employee.userid} message="This is what the employee will use to login." />
                    </div>

                    <div className="col50 padd-left-15">
                        <NonEditableTextField label="Date of Birth" value={birthDate} bold={true} />
                        <NonEditableTextField label="Gender" value={employee.gender} bold={true} />
                        <NonEditableTextField label="Nationality" value={employee.nationality} bold={true} />
                    </div>
                </Section>

                <Section>
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Status" value={employee.status} bold={true} />
                        <NonEditableTextField label="Date of Joining" value={formatDate(employee.joinedDate)} bold={true} />
                        <NonEditableTextField label="Employment Type" value={employee.employmentType.name} />
                    </div>
                    
                    <div className="col50 padd-left-15">
                        <NonEditableTextField label="Offer Date" value={formatDate(employee.offerDate)} />
                        <NonEditableTextField label="Comfirmed Date" value={formatDate(employee.confirmedDate)} />
                        <NonEditableTextField label="Contract End Date" value={formatDate(employee.contractEndDate)} />
                    </div>
                </Section>

                <Section>
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Branch" value={employee.branch.name} />
                        <NonEditableTextField label="Department" value={employee.department.name} />
                        <NonEditableTextField label="Designation" value={employee.designation.name} />
                        <NonEditableTextField label="Company Email" value={employee.companyEmail} />
                    </div>
                    
                    <div className="col50 padd-left-15">
                        <NonEditableTextField label="Reports To" value={employee.reportsTo.name}  />
                        <NonEditableTextField label="Salary Mode" value={employee.slaryMode} />
                        <NonEditableTextField label="Pay Scale" value={employee.payScale} />
                    </div>
                </Section>
            </div>
        )
    }
}

export default EmployeeView