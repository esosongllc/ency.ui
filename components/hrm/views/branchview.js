import React from 'react'
import { Section } from '../../../controls/layout'
import { HeadingLinks } from '../../../controls/HeadingLinks'
import { NonEditableTextField, NonEditableTextArea } from '../../../controls/input'

class BranchView extends React.Component {

    render() {
        const { branch } = this.props

        return (
            <div>
                <Section>

                </Section>

                <Section>
                    <div className="col50 padd-right-15">
                        <NonEditableTextField label="Branch" value={branch.name}  bold={true}/>
                    </div>

                    <div className="col50 padd-left-15">
                        <NonEditableTextArea label="Location" value={branch.location} />
                    </div>
                </Section>
            </div>
        )
    }

}

export default BranchView