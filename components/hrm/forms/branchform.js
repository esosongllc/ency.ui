import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Section } from '../../../controls/layout'
import { InputField, InputArea, RequiredValidator } from '../../../controls/input'

const BranchForm = (props) => {

    return (
        <div>
            <form>
                <Section>
                    <div className="col50 padd-right-15">
                        <Field type="text"
                            name="name"
                            label="Name"
                            component={InputField}
                            validate={RequiredValidator}
                        />
                    </div>

                    <div className="col50 padd-left-15">
                        <Field type="text"
                            name="location"
                            label="Location"
                            component={InputArea}
                        />
                    </div>
                </Section>
            </form>
        </div>
    )
}

export default reduxForm({
    form: 'BranchForm'
})(BranchForm)