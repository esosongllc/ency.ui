import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Section } from '../../../controls/layout'
import { CheckField, InputField, InputArea, RequiredValidator, RequiredNumberValidator } from '../../../controls/input'

const LeaveTypeForm = (props) => {

    return (
        <div>
            <form>
                <Section>
                    <div className="col50 padd-right-15">
                        <Field type="text"
                            name="name"
                            label="Name"
                            component={InputField}
                            validate={RequiredValidator}
                        />

                        <Field type="text"
                            name="carryforward"
                            label="Carry Forword"
                            component={CheckField}
                        />

                        <Field type="text"
                            name="withoutpay"
                            label="Leave Without Pay"
                            component={CheckField}
                        />
                        
                    </div>

                    <div className="col50 padd-left-15">
                        <Field type="text"
                            name="daysallowed"
                            label="Allowed Days"
                            component={InputField}
                            validate={RequiredNumberValidator}
                        />

                        <Field type="text"
                            name="allownegetive"
                            label="Exceed Allowed Days"
                            component={CheckField}
                        />

                        <Field type="text"
                            name="includeholidays"
                            label="Include Holidays"
                            component={CheckField}
                        />

                    </div>
                </Section>
            </form>
        </div>
    )
}

export default reduxForm({
    form: 'LeaveTypeForm'
})(LeaveTypeForm)