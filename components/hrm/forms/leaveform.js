// npm 
import React from 'react'
import { Field, reduxForm } from 'redux-form'

// ency
import {Section} from '../../../controls/layout'
import { TextBox, InputField, InputArea, DateField, ComboField, RequiredValidator, RequiredDateValidator } from '../../../controls/input'
import { ActionTypes, ActionControllers } from '../../../state/actions'
import { store } from '../../../state/store'

const status = [
    { name: 'Open' },
    { name: 'Approved' },
    { name: 'Rejected' }
]

class LeaveForm extends React.Component {
    constructor(props) {
        super(props)
    }


    render() {

        return (
            <div>
                <form>
                    <Section>
                        <div className="col50 padd-right-15">
                            <Field type="text"
                                name="employee"
                                label="Employee"
                                component={ComboField}
                                validate={RequiredValidator}
                                dataSource={this.props.employees}
                            /> 
                        </div>

                        <div className="col50 padd-left-15">
                            <Field type="text"
                                name="leavetype"
                                label="Leave Type"
                                component={ComboField}
                                validate={RequiredValidator}
                                dataSource={this.props.leaveTypes}
                            />

                            <Field type="text"
                                name="status"
                                label="Status"
                                component={ComboField}
                                dataSource={status}
                            />
                        </div>
                    </Section>

                    <Section>
                <div className="col50 padd-right-15">

                    <Field type="text"
                        name="fromdate"
                        label="From Date"
                        component={DateField}
                        validate={RequiredDateValidator}
                    />

                    <Field type="text"
                        name="todate"
                        label="To Date"
                        component={DateField}
                        validate={RequiredDateValidator}
                    />

                    <Field type="text"
                        name="submittedon"
                        label="Submit Date"
                        component={DateField}
                    />

                </div>

                <div className="col50 padd-left-15">
                   
                    <Field type="text"
                        name="reason"
                        label="Reason"
                        component={InputArea}
                    />

                </div>
            </Section>
                </form>
            </div>
        )
    }
}


export default reduxForm({ form: 'LeaveForm' })(LeaveForm)

