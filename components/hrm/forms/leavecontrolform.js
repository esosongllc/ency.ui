import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Section } from '../../../controls/layout'
import {
    DateField,
    InputField,
    InputArea,
    ComboField,
    RequiredValidator,
    RequiredDateValidator,
    RequiredNumberValidator
} from '../../../controls/input'

const LeaveControlForm = (props) => {

    return (
        <div>
            <form>
                <Section>
                    <div className="col50 padd-right-15">

                        <Field type="text"
                            name="leavetype"
                            label="Leave Type"
                            component={ComboField}
                            validate={RequiredValidator}
                            dataSource={props.leaveTypes}
                        />

                        <Field type="text"
                            name="days"
                            label="Allowed Days"
                            component={InputField}
                        />

                        <Field type="text"
                            name="fromdate"
                            label="From Date"
                            component={DateField}
                            validate={RequiredDateValidator}
                        />

                        <Field type="text"
                            name="todate"
                            label="To Date"
                            component={DateField}
                            validate={RequiredDateValidator}
                        />

                    </div>

                    <div className="col50 padd-left-15">
                        <Field type="text"
                            name="employmenttype"
                            label="Employment Type"
                            component={ComboField}
                            dataSource={props.employmentTypes}
                        />

                        <Field type="text"
                            name="branch"
                            label="Branch"
                            component={ComboField}
                            dataSource={props.branches}
                        />

                        <Field type="text"
                            name="department"
                            label="Department"
                            component={ComboField}
                            dataSource={props.departments}
                        />

                        <Field type="text"
                            name="designation"
                            label="Designation"
                            component={ComboField}
                            dataSource={props.designations}
                        />

                    </div>
                </Section>
            </form>
        </div>
    )
}

export default reduxForm({
    form: 'LeaveControlForm'
})(LeaveControlForm)