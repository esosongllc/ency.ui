import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Section } from '../../../controls/layout'
import { InputField, InputArea, ComboField, RequiredValidator } from '../../../controls/input'

const genders = [
    { id: 1, name: 'Male' },
    { id: 2, name: 'Female' },
    { id: 3, name: 'Others' }
]

const DepartmentForm = (props) => {

    return (
        <div>
            <form>
                <Section>
                    <div className="col50 padd-right-15">
                        <Field type="text"
                            name="name"
                            label="Department"
                            component={InputField}
                            validate={RequiredValidator}
                        />
                        <Field type="text"
                            name="description"
                            label="Description"
                            component={InputArea}
                        />
                    </div>

                    <div className="col50 padd-left-15">

                        <Field type="text"
                            name="leaveblock"
                            label="Leave Block"
                            component={ComboField}
                            dataSource={genders}
                        />

                        <Field type="text"
                            name="dd"
                            label="Description"
                            component={InputField}
                        />
                    </div>
                </Section>
            </form>
        </div>
    )
}

export default reduxForm({
    form: 'DepartmentForm'
})(DepartmentForm)