import React from 'react'
import { Field, reduxForm } from 'redux-form'
import Autocomplete from 'react-autocomplete'

import { Section } from '../../../controls/layout'
import { TextBox, InputField, InputArea, DateField, ComboField, RequiredValidator } from '../../../controls/input'
import { ActionTypes, ActionControllers } from '../../../state/actions'
import { store } from '../../../state/store'

const search = (item, value) => item.label.toLowerCase().indexOf(value.toLowerCase()) !== -1

const titles = [
    { name: 'Mr.' },
    { name: 'Mrs.' },
    { name: 'Ms.' },
    { name: 'Miss.' },
    { name: 'Dr.' },
    { name: 'Prof.' },
    { name: 'Madam.' },
    { name: 'Master.' },
    { name: 'Ph.D.' },
]

const genders = [
    { name: 'Male' },
    { name: 'Female' },
    { name: 'Others' }
]

const nationality = [
    { name: 'Ghanaian' },
    { name: 'Togolese' },
    { name: 'Others' }
]

const status = [
    { name: 'Active' },
    { name: 'Left' },
    { name: 'Other' }
]

const employmenttype = [
    { name: 'Apprentice' },
    { name: 'Intern' },
    { name: 'Contact' },
    { name: 'Full Time' },
    { name: 'Part Time' },
]

const salarymode = [
    { name: 'Bank' },
    { name: 'Cash' },
    { name: 'Cheque' }
]


const _EmployeeForm = (props) => (
    <div>
        <form>
            <Section>
                <div className="col50 padd-right-15">
                    <Field type="text"
                        name="title"
                        label="Title"
                        component={ComboField}
                        validate={RequiredValidator}
                        dataSource={titles}
                    />

                    <Field type="text"
                        name="lastname"
                        label="Surname"
                        component={InputField}
                        validate={RequiredValidator}
                    />

                    <Field type="text"
                        name="firstname"
                        label="First Name"
                        component={InputField}
                        validate={RequiredValidator}
                    />

                    <Field type="text"
                        name="middlename"
                        label="Middle Name"
                        component={InputField}
                    />

                </div>

                <div className="col50 padd-left-15">

                    <Field type="text"
                        name="birthdate"
                        label="Date of Birth"
                        component={DateField}
                    />

                    <Field type="text"
                        name="gender"
                        label="Gender"
                        component={ComboField}
                        validate={RequiredValidator}
                        dataSource={genders}
                    />

                    <Field type="text"
                        name="nationality"
                        label="Nationality"
                        component={ComboField}
                        validate={RequiredValidator}
                        dataSource={nationality}
                    />

                    <Field type="text"
                        name="userid"
                        label="Login Id"
                        component={InputField}
                    />

                </div>
            </Section>

            <Section>
                <div className="col50 padd-right-15">

                    <Field type="text"
                        name="status"
                        label="Status"
                        component={ComboField}
                        dataSource={status}
                    />

                    <Field type="text"
                        name="joineddate"
                        label="Date of Joining"
                        component={DateField}
                    />

                    <Field type="text"
                        name="employmenttype"
                        label="Employment Type"
                        component={ComboField}
                        dataSource={props.employmentTypes}
                    />

                </div>

                <div className="col50 padd-left-15">
                    <Field type="text"
                        name="offerdate"
                        label="Offer Date"
                        component={DateField}
                    />

                    <Field type="text"
                        name="offerconfirmeddate"
                        label="Confirmation Date"
                        component={DateField}
                    />

                    <Field type="text"
                        name="offerenddate"
                        label="Contact End Date"
                        component={DateField}
                    />

                </div>
            </Section>

            <Section>
                <div className="col50 padd-right-15">

                    <Field type="text"
                        name="branch"
                        label="Branch"
                        component={ComboField}
                        dataSource={props.branches}
                    />

                    <Field type="text"
                        name="department"
                        label="Department"
                        component={ComboField}
                        dataSource={props.departments}
                    />

                    <Field type="text"
                        name="designation"
                        label="Designation"
                        component={ComboField}
                        dataSource={props.designations}
                    />

                    <Field type="text"
                        name="companyemail"
                        label="Company Email"
                        component={InputField}
                    />

                </div>

                <div className="col50 padd-left-15">
                    <Field type="text"
                        name="reportsto"
                        label="Reports To"
                        component={ComboField}
                        dataSource={props.employees}
                    />

                    <Field type="text"
                        name="salarymode"
                        label="Salary Mode"
                        component={ComboField}
                        dataSource={salarymode}
                    />

                    <Field type="text"
                        name="payscale"
                        label="Pay Scale"
                        component={ComboField}
                        dataSource={salarymode}
                    />

                </div>
            </Section>

            <Section>
                <div className="col50 padd-right-15">

                    <Field type="text"
                        name="personalemail"
                        label="Personal Email"
                        component={InputField}
                    />

                    <Field type="text"
                        name="mobilephone"
                        label="Mobile Phone"
                        component={InputField}
                    />

                    <Field type="text"
                        name="homephone"
                        label="Home Phone"
                        component={InputField}
                    />

                    <Field type="text"
                        name="emergencycontact"
                        label="Emergency Contact"
                        component={InputField}
                    />

                    <Field type="text"
                        name="emergencyrelation"
                        label="Relation"
                        component={InputField}
                    />

                    <Field type="text"
                        name="emergencyphone"
                        label="Emergency Phone"
                        component={InputField}
                    />

                </div>

                <div className="col50 padd-left-15">
                    <Field type="text"
                        name="residence"
                        label="Residence"
                        component={InputField}
                    />

                    <Field type="text"
                        name="permanentaddress"
                        label="Permanent Address"
                        component={InputArea}
                    />



                </div>
            </Section>

        </form>
    </div>
)

const EmployeeForm = reduxForm({
    form: 'EmployeeForm'
})(_EmployeeForm)

export default EmployeeForm