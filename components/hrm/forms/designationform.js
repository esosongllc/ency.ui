import React from 'react'
import { Field, reduxForm } from 'redux-form'

import { Section } from '../../../controls/layout'
import { InputField, InputArea, RequiredValidator } from '../../../controls/input'

const DesignationForm = (props) => {

    return (
        <div>
            <form>
                <Section>
                    <div className="col50 padd-right-15">
                        <Field type="text"
                            name="name"
                            label="Designation"
                            component={InputField}
                            validate={RequiredValidator}
                        />
                    </div>

                    <div className="col50 padd-left-15">
                        <Field type="text"
                            name="description"
                            label="Description"
                            component={InputArea}
                        />
                    </div>
                </Section>
            </form>
        </div>
    )
}

export default reduxForm({ form: 'DesignationForm' })(DesignationForm)