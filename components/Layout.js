import Head from 'next/head'
import Link from 'next/link'
import TopBar from '../components/TopBar'
import MenuBar from '../components/MenuBar'

const Layout = ({ children, title, menu, breadcrumb, controls }) => (

    <div role="encyui" className="encyui">
        <div role="splashscree">

        </div>
        <div role="encyui-main" className="encyui-main">
            <Head>
                <title>  {title}  </title>
                <link rel="stylesheet" href="/static/css/font-awesome.css" />
                <link rel="stylesheet" href="/static/css/main.css" />
            </Head>
            <TopBar breadcrumb={breadcrumb} />
            <MenuBar name={menu} controls={controls} />


            <div role="encyui-main-view" className="encyui-main-view">

                <div role="encyui-main-ui-gutter" className="encyui-main-ui-gutter">

                </div>

                {children}

                <div role="encyui-main-ui-gutter" className="encyui-main-ui-gutter">

                </div>
            </div>
        </div>
    </div>
)

export default Layout