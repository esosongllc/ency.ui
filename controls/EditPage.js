import React from 'react'
import { SubmitButton, Button } from './input'

class EditPage extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { children, form, onClose } = this.props
        return (
            <div role="encyui-main-ui-right" className="encyui-main-ui-right">
                <div role="content" className="ui-right-content">
                    <div role="controls" className="ui-right-controlbar">
                        <div role="left-side"></div>
                        <div role="right-side" className="right-side">
                            <Button label="Cancel" cancel={true} cb={() => onClose()} />
                            <div>
                                <SubmitButton label="Save" form={form} />
                            </div>
                        </div>
                    </div>

                    {children}
                </div>

                {/* <div role="footer" className="ui-right-footer">
                    <div role="right-side" className="right-side">
                        <div>
                            <SubmitButton label="Save" form={form} />
                        </div>
                    </div>
                </div> */}
            </div>
        )
    }
}

export default EditPage