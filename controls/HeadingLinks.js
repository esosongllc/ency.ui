import Link from 'next/link'
import uuid from 'uuid'

const HeadingLinks = (props) => (
    <div className="ui-left-headlink col50">
        <div>
            <span>{props.label}</span>
        </div>
        <div>
            {props.links.map((link) => (
                <div key={uuid()}>
                    <Link as={link.url} href={link.url}>
                        <a> {link.label}</a>
                    </Link>
                </div>
            ))}
        </div>

    </div>
)

class HeadingIcons extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        const style = {
            color: 'rgba(80, 114, 153, 0.7)',
            fontSize: '16px'
        }

        return (
            <div className="ui-left-headicon ">
                <div>
                    <span>{this.props.label}</span>
                </div>
                <div>
                    {this.props.links.map((link) => (
                        <div key={uuid()}>
                            <Link as={link.url} href={link.url}>
                                <a>
                                    <div>
                                        <span className="headicon-box" >
                                            <i className={`fa ${link.icon} `} style={style}></i>
                                        </span>
                                        <span className="headicon-label">{link.label}</span>
                                    </div>
                                </a>
                            </Link>
                        </div>
                    ))}
                </div>

            </div>
        )
    }
}

export {
    HeadingLinks,
    HeadingIcons
}