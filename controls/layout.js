import React from 'react'
import Modal from 'react-modal'
import { Button } from './input'

const Page = (props) => {

    let _render = null

    if (props.aside !== undefined && !props.aside) {
        _render = { display: 'none' }
    }

    return (
        <div role="encyui-main-ui-left" className=" encyui-center-view encyui-main-ui-left ">
            <div role="ui-left-aside" style={_render} className="ui-left-aside">
                {props.asideContent}
            </div>
            <div role="ui-left-content" className="ui-left-content">
                {props.children}
            </div>
        </div>
    )
}

const Section = (props) => (
    <div className="ui-section">
        {props.children}

    </div>
)



const SavingDialog = ({ isOpen }) => {

    const styles = {
        content: {
            top: '50%',
            left: '50%',
            right: 'auto',
            bottom: 'auto',
            marginRight: '-50%',
            transform: 'translate(-50%, -50%)',
            border: 'none',
            background: 'none',
            fontSize: '18px',
            fontWeight: '300'
        }
    }

    return (
        <Modal isOpen={isOpen} style={styles} contentLabel="Example Modal" >
            <p>Saving...</p>
        </Modal>
    )
}

const LoadingDialog = (props) => {
    const styles = {
        content: {
        }
    }

    return (
        <div className="encyui-loading-dialog" style={styles}>Loading...</div>
    )
}

const Dialog = (props) => {

    const styles = {
        content: {
            borderRadius: 'none',
            border: 'none',
            padding: 0,
            top: '35%',
            width: '200px',
            margin: '0 auto',
            bottom: 'none',
            lineHeight: '18px',
            width: '450px',
            background: 'none'
        },
        overlay: {
            background: 'rgba(0, 0, 0, 0.7)'
        }
    }

    const headerStyle = {
        background: props.for.match(/error/i) ? '#ff6e6e' : '#6287ae'
    }

    return (
        <Modal isOpen={props.isOpen} style={styles} contentLabel="Error Modal" >
            <div className="encyui-dialog">
                <div role="header" style={headerStyle}>
                    <h4>{props.title}</h4>
                    <a style={{ color: '#fff', pointer: 'hand' }}>
                        <i className="fa fa-close" onClick={props.closeCb}></i>
                    </a>
                </div>
                <div role="content">
                    {props.children}
                </div>
                <div role="footer">
                    {props.buttons}
                </div>
            </div>
        </Modal>
    )
}

const FetchErrorDialog = (props) => {
    const dialogButton = <Button label="Try Again!"
        cancel={false}
        cb={() => location.reload()} />

    return (
        <Dialog isOpen={props.isOpen}
            title="Fetch Error" for="error"
            buttons={dialogButton}
            closeCb={props.closeCb}>
            <p>Uh oh, an error was encountered</p>
        </Dialog>
    )
}


export {
    Dialog,
    LoadingDialog,
    Page,
    Section,
    SavingDialog,
    FetchErrorDialog
}