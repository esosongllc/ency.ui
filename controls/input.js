import { connect } from 'react-redux'
import { submit } from 'redux-form'
import Link from 'next/link'
import uuid from 'uuid/v4'
import Autosuggest from 'react-autosuggest'
import DateTime from 'react-datetime'
import moment from 'moment'

// TextBox
const TextBox = ({ type = "text", name, label, placeholder, message = '' }) => (
    <div className="encyui-formcontrol">
        <div><label htmlFor={name}>{label}</label></div>
        <div><input type={type} name={name} placeholder={placeholder} /></div>
        <div><span>{message}</span></div>
    </div>
)



// Button
const Button = ({ label, cancel, cb }) => {
    const className = cancel ? "encyui-button encyui-button-cancel" : "encyui-button"
    return (
        <span className={className} onClick={cb}>
            {label}
        </span>
    )
}

const IconButton = ({ label, cancel, iconClass, cb }) => {
    const className = cancel ? "encyui-button encyui-button-cancel" : "encyui-button"
    return (
        <div className={className} onClick={cb}>
            <span style={{ marginRight: '5px' }}>{label}</span>
            <i className={iconClass} aria-hidden="true"></i>
        </div>
    )
}

const NewButton = ({ page }) => {
    const url = `${page}/new`

    return (
        <div>
            <Link prefetch as={url} href={url} >
                <a className="encyui-button">New</a>
            </Link>
        </div>
    )
}

// Submit Button, gets a submit from redux-from and disptach it
let _submitButton = ({ dispatch, label, form }) => (
    <div>
        <div role="submit button" className="encyui-button" onClick={() => dispatch(submit(form))}>
            {label}
        </div>
    </div>
)

const SubmitButton = connect()(_submitButton)


// use by redux form
class InputField extends React.Component {

    render() {
        const field = this.props

        return (
            <div className="encyui-formcontrol">
                <div>
                    <label htmlFor={field.input.name}>{field.label}</label>
                </div>
                <div>
                    <input {...field.input} type={field.type} placeholder={field.placeholder} />
                </div>
                <div>
                    {field.meta.touched &&
                        (field.meta.error &&
                            <span style={{ color: 'red' }}>
                                {field.meta.error}
                            </span>
                        )
                    }
                </div>
            </div>
        )
    }
}

const InputArea = (field) => (
    <div className="encyui-formcontrol">
        <div>
            <label htmlFor={field.input.name}>{field.label}</label>
        </div>
        <div>
            <textarea {...field.input} rows="5" ></textarea>
        </div>
        <div>
            {field.meta.touched &&
                (field.meta.error &&
                    <span style={{ color: 'red' }}>
                        {field.meta.error}
                    </span>
                )
            }
        </div>
    </div>
)

export const CheckField = (field) => {

    return (
        <div className="encyui-formcontrol">
            <div>
                <input {...field.input} type="checkbox"
                    style={
                        {
                            width: '16px',
                            height: '16px',
                            verticalAlign: 'bottom'
                        }
                    }
                />
                <label htmlFor={field.input.name}
                    style={
                        { width: '150px', marginLeft: '10px' }
                    }  >{field.label}</label>
            </div>
            <div>
                {field.meta.touched &&
                    (field.meta.error &&
                        <span style={{ color: 'red' }}>
                            {field.meta.error}
                        </span>
                    )
                }
            </div>
        </div>
    )
}

// 
const NonEditableTextField = ({ label, value, bold, message }) => {
    const style = bold ?
        {
            fontWeight: '700',
            background: 'rgba(237, 238, 240, 0.2)'
        } :
        { fontWeight: 'normal' }

    const fieldValue = value ? value : ''

    return (
        <div className="encyui-formcontrol">
            <div>
                <label>{label}</label>
            </div>
            <div>
                <input type="text" value={fieldValue} style={style} readOnly />
            </div>
            <div>
                <span>{message}</span>
            </div>
        </div>
    )
}

const NonEditableTextArea = ({ label, value, message }) => {

    const fieldValue = value ? value : ''
    return (
        <div className="encyui-formcontrol">
            <div>
                <label >{label}</label>
            </div>
            <div>
                <textarea value={fieldValue} rows="5" readOnly></textarea>
            </div>
            <div>
                <span>{message}</span>
            </div>
        </div>
    )
}

export const NonEditableCheckField = ({ label, value, message }) => {

    const fieldValue = value ? 'Yes' : 'No'

    return (
        <div className="encyui-formcontrol">
            <div>
                <label>{label}</label>
            </div>
            <div>
                <input type="text" value={fieldValue}  readOnly />
            </div>
            <div>
                <span>{message}</span>
            </div>
        </div>
    )
}

export const NonEditableCheckField2 = ({ label, value, message }) => {

    const fieldValue = value ? value : ''
    const style = {
        width: '16px',
        height: '16px',
        verticalAlign: 'bottom'
    }
    console.log(fieldValue)
    return (
        <div className="encyui-formcontrol">
            <div></div>
            <div>
                <input type="checkbox" style={style} value={fieldValue} />
                <label style={{ width: '150px', marginLeft: '10px' }}  >{label}</label>
            </div>
            <div>
                <span>{message}</span>
            </div>
        </div>
    )
}

class DateField extends React.Component {


    render() {
        const field = this.props

        const defaultValue = field.defaultValue ?
            moment(field.defaultValue).format('DD-MMM-YYYY') : ''

        return (
            <div className="encyui-formcontrol">
                <div>
                    <label htmlFor={field.input.name}>{field.label}</label>
                </div>
                <div>
                    <DateTime viewMode="days"
                        dateFormat="DD-MMM-YYYY"
                        timeFormat={false}
                        closeOnSelect={true}
                        onChange={(d) => field.input.onChange(d)}
                        onBlur={() => field.input.onBlur()}
                        inputProps={{ name: field.input.name }}
                        defaultValue={defaultValue}
                    />
                </div>
                <div>
                    {field.meta.touched &&
                        (field.meta.error &&
                            <span style={{ color: 'red' }}>
                                {field.meta.error}
                            </span>
                        )
                    }
                </div>
            </div>
        )
    }
}

class ComboField extends React.Component {
    constructor(props) {
        super(props)

        let field = this.props

        this.state = {
            value: '',
            suggestions: [],
            dataSource: this.props.dataSource,
            defaultValue: field.input.value
        }
    }

    componentDidMount() {
        this.setState({
            value: this.state.defaultValue
        })
    }

    getSuggestions = ({ value }) => {

        const inputVal = value.trim().toLowerCase()
        const lenght = inputVal.lenght

        return this.state.dataSource.filter((f) => f.name.toLowerCase().indexOf(inputVal) !== -1)
    }

    getSuggestionValue = (s) => {
        const { id, name } = s
        var el = document.getElementById(this.props.input.name)
        el.focus()

        // not all list have id field
        if (undefined === id) {
            el.value = name
        } else {
            el.value = id
        }

        el.blur()

        return name
    }

    onChange = (event, { newValue }) => {
        this.setState({
            value: newValue
        })
    }

    render() {
        const field = this.props
        const { value, suggestions } = this.state

        const inputProps = {
            name: '',
            value,
            onChange: this.onChange
        }

        return (
            <div className="encyui-formcontrol">
                <div>
                    <label htmlFor={field.input.name}>{field.label}</label>
                </div>
                <div>
                    <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={(v) => this.setState({ suggestions: this.getSuggestions(v) })}
                        onSuggestionsClearRequested={() => this.setState({ suggestions: [] })}
                        getSuggestionValue={(s) => this.getSuggestionValue(s)}
                        renderSuggestion={(s) => <div>{s.name}</div>}
                        shouldRenderSuggestions={() => true}
                        inputProps={inputProps} />
                    <input className="encyui-formcontrol-hidden" style={{ width: '350px' }} {...field.input} id={field.input.name} />
                </div>
                <div>
                    {field.meta.touched &&
                        (field.meta.error &&
                            <span style={{ color: 'red' }}>
                                {field.meta.error}
                            </span>
                        )
                    }
                </div>
            </div>
        )
    }
}

// DataGrid
export class DataGrid extends React.Component {

    renderRow(r, columnNames, style) {

        var row = columnNames.map((n) => this.renderCell(r, n, style))
        return (
            <div key={uuid()} className="encyui-datagrid-row">
                {row}
            </div>
        )
    }
    // r is record, n is column and it's props
    renderCell = (r, n, style) => {

        var cell = null

        if (n.merge) { cell = r[n.name[0]] + ' ' + r[n.name[1]] }
        else if (n.subname) { cell = r[n.name][n.subname] }
        else { cell = r[n.name] }


        if(n.type && n.type === 'date'){
            cell = moment(cell).format('DD-MMM-YYYY')
        }

        const uri = `${this.props.pathname}/edit?id=${r.id}`

        if (n.link !== undefined && n.link === true) {
            cell = (
                <Link prefetch as={uri} href={uri}>
                    <a>{cell}</a>
                </Link>
            )
        }
        return (
            <div key={uuid()} style={style}> {cell} </div>
        )
    }

    render() {
        const { columns, data } = this.props

        const dataList = Array.from(Object.keys(data), k => data[k])

        const style = { width: `${100 / columns.length}%` }

        const col = columns.map((c) => <div style={style} key={uuid()}>{c.text}</div>)

        const row = dataList.map((r) => this.renderRow(r, columns, style))

        return (
            <div role="DataGrid" className="encyui-datagrid" >
                <div role="DataGridHeader" className="encyui-datagrid-header">
                    {col}
                </div>
                <div role="DataGridBody" className="encyui-datagrid-body">
                    {row}
                </div>
            </div>
        )
    }
}

export const Breadcrumb = (props) => (
    <ul className="ui-left-headlink ui-breadcrumb">
        {props.links.map((link) => (
            <li key={uuid()}>
                <Link as={link.url} href={link.url}>
                    <a> {link.label}</a>
                </Link>
            </li>
        ))}
    </ul>
)

export const RequiredValidator = (value) => ((value && value.trim()) ? undefined : 'Field is Required')

export const RequiredDateValidator = (value) => {

    if (value) {
        var dt = new Date(value)

        if (dt.toString() === 'Invalid Date') {
            return 'Invalid Date'
        }

        return undefined

    } else {
        return 'Field is Required'
    }
}

export const RequiredNumberValidator = (value) => {

    if (value) {
        var ex = new RegExp(/^\d+$/)
        if (!ex.test(value)) {
            return "Invalid Input, Numbers Only"
        }
        return undefined
    } else {
        return 'Field is Required'
    }

}

export {
    TextBox,
    Button,
    IconButton,
    InputField,
    InputArea,
    NonEditableTextField,
    NonEditableTextArea,
    DateField,
    ComboField,
    NewButton,
    SubmitButton
} 
