import {Section} from './layout'
import {HeadingIcons} from './HeadingLinks'

export default (
    <Section>
        <HeadingIcons label='Apps' links={[
            { url: '/hrm', label: 'Human Resource', icon: 'fa-users', color: '#0e7eca' },
            { url: '/hrm', label: 'Task', icon: 'fa-tasks', color: '#fb3eba' },
            { url: '/hrm', label: 'Settings', icon: 'fa-wrench', color: '#848383' }
        ]} />
    </Section>
)