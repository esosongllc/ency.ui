
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import DesignationForm from '../../../components/hrm/forms/designationform'
import { designation_post } from '../../../controllers/apicalls'

const { dispatch } = makeStore()

class DesignationNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        dispatch({ type: ActionTypes.RESET_DESIGNATION, payload: [] })
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(designation_post, values)

        post.then(({ data }) => {
            Router.push(`/hrm/designations/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {
        const _title = 'New Designation'
        const _menu = 'New Designation'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/designations', label: 'Designation' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="DesignationForm" />
                </div>
            </div>
        )

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    <DesignationForm id="newDesignationForm" onSubmit={this.handleSubmit} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

export default withRedux(makeStore)(DesignationNew)