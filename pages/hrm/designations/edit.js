// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { Section } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { NonEditableTextField, NonEditableTextArea } from '../../../controls/input'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import { GetById, fnFetchDesignation } from '../../../controllers/functions'

const { dispatch } = makeStore()

class DesignationEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let { url, states } = this.props

        var designation = GetById(states.designations, url.query.id)
        if (undefined === designation || null === designation) {
            this.props.onFetchDesignation(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_DESIGNATION, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/designations', label: 'Designation' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Designation'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const designation = GetById(states.designations, url.query.id)

            _title = `${designation.name} - ${_menu}`
            _menu = (<div>{designation.name}</div>)

            _buttons = (
                <div></div>
            )

            _render = (
                <div>
                    <Section>

                    </Section>

                    <Section>
                        <div className="col50 padd-right-15">
                            <NonEditableTextField label="Department" value={designation.name} bold={true} />
                        </div>

                        <div className="col50 padd-left-15">
                            <NonEditableTextArea label="Description" value={designation.description} />
                        </div>
                    </Section>
                </div>
            )

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.designationsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchDesignation: bindActionCreators(fnFetchDesignation, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(DesignationEdit)
