import React from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { designation_get } from '../../../controllers/apicalls'
import { DataGrid } from '../../../controls/input'
import { fnFetchDesignations } from '../../../controllers/functions'

const { dispatch } = makeStore()

class DesignationIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.onFetchDesignations()
    }

    render() {

        const { states, url } = this.props
        const _title = 'Designations'
        const _menu = 'Designations'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render department type list
            let columns = [
                { text: 'Designation', name: 'name', link: true },
                { text: '', name: 'name' },
                { text: '', name: 'key' }
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.designations} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        states: state.designationsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchDesignations: bindActionCreators(fnFetchDesignations, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(DesignationIndex)