
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog, LoadingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import LeaveControlForm from '../../../components/hrm/forms/leavecontrolform'
import { leavecontrol_endpoint } from '../../../controllers/apicalls'

import {
    fnFetchEmploymentTypes,
    fnFetchBranches,
    fnFetchDepartments,
    fnFetchDesignations,
    fnFetchLeaveTypes,

    convertEmployeeObjectToArray
} from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveControlNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillMount() {
        this.props.onFetchEmploymentTypes()
        this.props.onFetchBranches()
        this.props.onFetchDepartments()
        this.props.onFetchDesignations()
        this.props.onFetchLeaveTypes()
    }

    componentDidMount() {
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(leavecontrol_endpoint, values)

        post.then(({ data }) => {
            dispatch({ type: ActionTypes.RESET_LEAVECONTROL, payload: [] })
            Router.push(`/hrm/leavecontrols/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {

        const {
            employmentTypesReducer,
            branchesReducer,
            deaprtmentsReducer,
            designationsReducer,
            leaveTypesReducer,
            url
        } = this.props

        const _title = 'New Leave Control'
        const _menu = 'New Leave Control'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leavecontrols', label: 'Leave Controls' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="LeaveControlForm" />
                </div>
            </div>
        )

        let _render = null

        if (employmentTypesReducer.fetched &&
            branchesReducer.fetched &&
            deaprtmentsReducer.fetched &&
            designationsReducer.fetched &&
            leaveTypesReducer.fetched) {
            
            _render = <LeaveControlForm
                id="newLveCtrlForm"
                onSubmit={this.handleSubmit}
                employmentTypes={Object.values(employmentTypesReducer.employmentTypes)}
                branches={Object.values(branchesReducer.branches)}
                departments={Object.values(deaprtmentsReducer.departments)}
                designations={Object.values(designationsReducer.designations)}
                leaveTypes={Object.values(leaveTypesReducer.leavetypes)}
            />

        } else {
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        employmentTypesReducer: state.employmentTypesReducer,
        branchesReducer: state.branchesReducer,
        deaprtmentsReducer: state.deaprtmentsReducer,
        designationsReducer: state.designationsReducer,
        leaveTypesReducer: state.leaveTypesReducer,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmploymentTypes: bindActionCreators(fnFetchEmploymentTypes, dispatch),
        onFetchBranches: bindActionCreators(fnFetchBranches, dispatch),
        onFetchDepartments: bindActionCreators(fnFetchDepartments, dispatch),
        onFetchDesignations: bindActionCreators(fnFetchDesignations, dispatch),
        onFetchLeaveTypes: bindActionCreators(fnFetchLeaveTypes, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveControlNew)