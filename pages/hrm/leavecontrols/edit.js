// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import moment from 'moment'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { Section } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton, NonEditableTextField } from '../../../controls/input'
import { GetById, fnFetchLeaveControl } from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveControlEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let { url, states } = this.props

        var leaveControl = GetById(states.leavecontrols, url.query.id)
        if (undefined === leaveControl || null === leaveControl) {
            this.props.onFetchLeaveControl(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_LEAVECONTROL, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leavecontrols', label: 'Leave Controls' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Leave Control'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const leaveControl = GetById(states.leavecontrols, url.query.id)

            _title = `${leaveControl.code} - ${_menu}`
            _menu = (<div>{leaveControl.code}</div>)

            _buttons = (
                <div></div>
            )

            const fromDate = moment(leaveControl.fromDate).format('DD-MMM-YYYY')
            const toDate = moment(leaveControl.toDate).format('DD-MMM-YYYY')

            _render = (
                <div>
                    <Section>

                    </Section>

                    <Section>
                        <div className="col50 padd-right-15">
                            <NonEditableTextField label="Code" value={leaveControl.code} />
                            <NonEditableTextField label="Leave Type" value={leaveControl.leaveType.name} bold={true} />
                            <NonEditableTextField label="From Date" value={fromDate} bold={true} />
                            <NonEditableTextField label="To Date" value={toDate} bold={true} />
                        </div>

                        <div className="col50 padd-left-15">
                            <NonEditableTextField label="Employment Type" value={leaveControl.employmentType.name} />
                            <NonEditableTextField label="Branch" value={leaveControl.branch.name} />
                            <NonEditableTextField label="Department" value={leaveControl.department.name} />
                            <NonEditableTextField label="Designation" value={leaveControl.designation.name} />
                        </div>
                    </Section>
                </div>
            )

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.leaveControlsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchLeaveControl: bindActionCreators(fnFetchLeaveControl, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveControlEdit)
