import React from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { DataGrid } from '../../../controls/input'
import { fnFetchLeaveControls } from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveControlIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.onFetchLeaveControls()
    }

    handleDialogClose() {
        // fake receieve to we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_LEAVECONTROL, payload: [] })
    }

    render() {
        const { states, url } = this.props
        const _title = 'Leave Control'
        const _menu = 'Leave Control'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render employment type list
            let columns = [
                { text: 'Leave Allocation', name: 'code', link: true },
                { text: 'Leave Type', name: 'leaveType', subname: 'name' },
                { text: 'From Date', name: 'fromDate', type: 'date' },
                { text: 'To Date', name: 'toDate', type: 'date' },
                { text: '', name: 'leaveType', subname: 'daysAllowed' },
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.leavecontrols} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error}
                        closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.leaveControlsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchLeaveControls: bindActionCreators(fnFetchLeaveControls, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveControlIndex)