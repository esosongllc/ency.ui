// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { Section } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { NonEditableTextField, NonEditableCheckField } from '../../../controls/input'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import { GetById, fnFetchLeaveType } from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveTypeEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let { url, states } = this.props

        var leaveType = GetById(states.leavetypes, url.query.id)
        if (undefined === leaveType || null === leaveType) {
            this.props.onFetchLeaveType(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_LEAVETYPE, payload: [] })
    }

    render() {

        const { states, url } = this.props

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leavetypes', label: 'Leave Types' }
            ]} />
        )

        let _title = null
        let _render = null

        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Leave Type'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const leaveType = GetById(states.leavetypes, url.query.id)

            _title = `${leaveType.name} - ${_menu}`
            _menu = (<div>{leaveType.name}</div>)

            _buttons = (
                <div></div>
            )

            _render = (
                <div>
                    <Section>

                    </Section>

                    <Section>
                        <div className="col50 padd-right-15">
                            <NonEditableTextField label="Leave Type" value={leaveType.name} bold={true} />
                            <NonEditableTextField label="Allowed Days" value={leaveType.daysAllowed} />
                            <NonEditableCheckField label="Exceed Allowed Days" value={leaveType.allowNegetive} />
                        </div>

                        <div className="col50 padd-left-15">
                            <NonEditableCheckField label="Leave Without Pay" value={leaveType.withoutPay} />
                            <NonEditableCheckField label="Carry Forward" value={leaveType.carryForward} />
                            <NonEditableCheckField label="Include Holidays" value={leaveType.includeHolidays} />

                        </div>
                    </Section>
                </div>
            )


        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.leaveTypesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchLeaveType: bindActionCreators(fnFetchLeaveType, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveTypeEdit)
