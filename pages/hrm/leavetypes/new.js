
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import LeaveTypeForm from '../../../components/hrm/forms/leavetypeform'
import { leavetype_endpoint } from '../../../controllers/apicalls'

const { dispatch } = makeStore()

class LeaveTypeNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(leavetype_endpoint, values)

        post.then(({ data }) => {
            dispatch({ type: ActionTypes.RESET_LEAVETYPE, payload: [] })
            Router.push(`/hrm/leavetypes/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {
        const _title = 'New Leave Type'
        const _menu = 'New Leave Type'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leavetypes', label: 'Leave Types' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="LeaveTypeForm" />
                </div>
            </div>
        )

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    <LeaveTypeForm id="newLevTypForm" onSubmit={this.handleSubmit} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

export default withRedux(makeStore)(LeaveTypeNew)