import React from 'react'
import Link from 'next/link'
import { bindActionCreators } from 'redux'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'


import Layout from '../../components/Layout'
import { Page, Section } from '../../controls/layout'
import AppAside from '../../controls/appaside'
import { HeadingLinks, HeadingIcons } from '../../controls/HeadingLinks'
import { makeStore } from '../../state/store'
import { ActionTypes } from '../../state/actions'

class HrmIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        let { employees } = this.props

        // only request for employees if list is empty
        if (employees.length < 1) {

            // these no employee, so fetch
            this.props.onFetchEmployees()
        }
    }

    render() {
        const breadcrumb = (
            <Link href='/'>
                <a>Dashboard</a>
            </Link>
        )

        return (
            <Layout title="Human Resource" menu="Human Resource" breadcrumb={breadcrumb}>
                <Page aside={true} asideContent={AppAside}>
                    <Section>
                        <HeadingLinks label="Employee and Attendance" links={[
                            { url: '/hrm/employees', label: 'Employees' },
                            { url: '/hrm/employeetools', label: 'Employees Tools' },
                            { url: '/hrm/attendance', label: 'Attendance' },
                            { url: '/hrm/uploadattendance', label: 'Upload Attendance' },
                        ]} />

                        <HeadingLinks label="Recruitment" links={[
                            { url: '/hrm/jobapplication', label: 'Job Applications' },
                            { url: '/hrm/jobopenings', label: 'Job Openings' },
                            { url: '/hrm/offerletters', label: 'Offer Letter' }
                        ]} />
                    </Section>

                    <Section>
                        <HeadingLinks label="Leaves and Holidays" links={[
                            { url: '/hrm/leaves', label: 'Leaves' },
                            { url: '/hrm/leavetypes', label: 'Leave Type' },
                            { url: '/hrm/holidaylist', label: 'Holiday List' },
                            { url: '/hrm/leavecontrols', label: 'Leave Allocation' },
                            { url: '/hrm/blockedleave', label: 'Blocked Leave' }
                        ]} />

                        <HeadingLinks label="Payroll" links={[
                            { url: '/hrm/salaryslips', label: 'Salary Slip' },
                            { url: '/hrm/salarystructures', label: 'Salary Structure' },
                            { url: '/hrm/payrolltools', label: 'Payroll Tools' }
                        ]} />
                    </Section>

                    <Section>
                        <HeadingLinks label="Settings" links={[
                            { url: '/hrm/employmenttypes', label: 'Employment Type' },
                            { url: '/hrm/branches', label: 'Branch' },
                            { url: '/hrm/departments', label: 'Department' },
                            { url: '/hrm/designations', label: 'Designation' },
                        ]} />

                        <HeadingLinks label=" " links={[
                            { url: '/hrm/status', label: 'Status' },
                            { url: '/hrm/salarymodes', label: 'Salary Mode' },
                            { url: '/hrm/payscales', label: 'PayScale' },
                            { url: '/nationality', label: 'Nationality' },
                        ]} />
                    </Section>

                    <Section>
                        <HeadingLinks label="Reports" links={[
                            { url: '/reports?type=lbalance', label: 'Employee Leave Balance' },
                            { url: '/reports?type=ebirthday', label: 'Employee Birthday' },

                            
                        ]} />

                        <HeadingLinks label="Help" links={[
                            { url: '/help?content=leavemanagement', label: 'Leave Management' },
                        ]} />
                    </Section>
                </Page>
            </Layout>
        )
    }
}


const cbFetchEmployees = () => {
    const request = axios.get('http://localhost:59871/api/employees')
    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: ActionTypes.FETCH_EMPLOYEES, payload: data })
        })
    }
}

const mapStateToProps = (state) => {
    return {
        employees: state.employeesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployees: bindActionCreators(cbFetchEmployees, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(HrmIndex)