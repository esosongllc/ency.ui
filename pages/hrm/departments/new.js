
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import DepartmentForm from '../../../components/hrm/forms/departmentform'
import { department_post } from '../../../controllers/apicalls'

const { dispatch } = makeStore()

class DepartmentNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        dispatch({ type: ActionTypes.RESET_DEPARTMENT, payload: [] })
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(department_post, values)

        post.then(({ data }) => {
            Router.push(`/hrm/departments/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {
        const _title = 'New Department'
        const _menu = 'New Department'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/departments', label: 'Department' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="DepartmentForm" />
                </div>
            </div>
        )

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    <DepartmentForm id="newDepartmentForm" onSubmit={this.handleSubmit} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

export default withRedux(makeStore)(DepartmentNew)