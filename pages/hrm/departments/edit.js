// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import DepartmentView from '../../../components/hrm/views/departmentview'
import { department_get } from '../../../controllers/apicalls'
import { GetById, fnFetchDepartment } from '../../../controllers/functions'

const { dispatch } = makeStore()

class DepartmentEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let { url, states } = this.props

        var department = GetById(states.departments, url.query.id)
        if (undefined === department || null === department) {
            this.props.onFetchDepartment(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_DEPARTMENT, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/departments', label: 'Department' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Department'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const department = GetById(states.departments, url.query.id)

            _title = `${department.name} - ${_menu}`
            _menu = (<div>{department.name}</div>)

            _buttons = (
                <div></div>
            )

            _render = <DepartmentView department={department} />

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.deaprtmentsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchDepartment: bindActionCreators(fnFetchDepartment, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(DepartmentEdit)
