import React from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { department_get } from '../../../controllers/apicalls'
import { DataGrid } from '../../../controls/input'
import { fnFetchDepartments } from '../../../controllers/functions'

const { dispatch } = makeStore()

class DepartmentIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.onFetchDepartments()
    }

    handleDialogClose() {
        // fake receieve to we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_DEPARTMENT, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const _title = 'Departments'
        const _menu = 'Departments'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render department type list
            let columns = [
                { text: 'Department', name: 'name', link: true },
                { text: '', name: 'name' },
                { text: '', name: 'key' }
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.departments} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error}
                        closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        states: state.deaprtmentsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchDepartments: bindActionCreators(fnFetchDepartments, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(DepartmentIndex)