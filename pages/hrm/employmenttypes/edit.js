// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import EmployeeView from '../../../components/hrm/views/employeeview'
import BasicInfo from '../../../components/hrm/employees/basicinfo'
import EmploymentTypeView from '../../../components/hrm/views/employmenttypeview'
import { employmenttype_get } from '../../../controllers/apicalls'
import { GetById, fnFetchEmploymentType } from '../../../controllers/functions'

const { dispatch } = makeStore()

class EmploymentTypeEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let {url, states} = this.props

        var employmentType = GetById(states.employmentTypes, url.query.id)
        if(undefined === employmentType || null === employmentType){
            this.props.onFetchEmploymentType(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_EMPLOYMENTTYPE, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/employmenttypes', label: 'Employment Types' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Employment Type'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const employmentType = GetById(states.employmentTypes, url.query.id)

            _title = `${employmentType.name} - ${_menu}`
            _menu = (<div>{employmentType.name}</div>)

            _buttons = (
                <div></div>
            )

            _render = <EmploymentTypeView employmentType={employmentType} />

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.employmentTypesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmploymentType: bindActionCreators(fnFetchEmploymentType, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(EmploymentTypeEdit)
