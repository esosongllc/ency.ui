
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import EmploymentTypeForm from '../../../components/hrm/forms/employmenttypeform'
import { employmenttype_post } from '../../../controllers/apicalls'

const { dispatch } = makeStore()

class EmploymentTypeNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        dispatch({ type: ActionTypes.RESET_EMPLOYMENTTYPE, payload: [] })
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(employmenttype_post, values)

        post.then(({ data }) => {
            Router.push(`/hrm/employmenttypes/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {
        const _title = 'New Employment Type'
        const _menu = 'New Employment Type'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/employmenttypes', label: 'Employment Types' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="EmploymentTypeForm" />
                </div>
            </div>
        )

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    <EmploymentTypeForm id="newEmpTypForm" onSubmit={this.handleSubmit} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

export default withRedux(makeStore)(EmploymentTypeNew)