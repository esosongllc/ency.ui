import React from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { DataGrid } from '../../../controls/input'
import { fnFetchEmploymentTypes } from '../../../controllers/functions'

const { dispatch } = makeStore()

class EmploymentTypeIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.onFetchEmploymentTypes()
    }

    handleDialogClose() {
        // fake receieve to we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_EMPLOYMENTTYPE, payload: [] })
    }

    render() {
        const { states, url } = this.props
        const _title = 'Employment Types'
        const _menu = 'Employment Types'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render employment type list
            let columns = [
                { text: 'Employment Type', name: 'name', link: true },
                { text: '', name: 'name' },
                { text: '', name: 'key' }
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.employmentTypes} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error}
                        closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.employmentTypesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmploymentTypes: bindActionCreators(fnFetchEmploymentTypes, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(EmploymentTypeIndex)