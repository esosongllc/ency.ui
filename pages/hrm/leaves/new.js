
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog, LoadingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import LeaveForm from '../../../components/hrm/forms/leaveform'
import { leave_endpoint } from '../../../controllers/apicalls'
import {
    fnFetchEmployees,
    fnFetchLeaveTypes,

    convertEmployeeObjectToArray
} from '../../../controllers/functions';

const { dispatch } = makeStore()

class LeaveNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentWillMount() {
        this.props.onFetchEmployees()
        this.props.onFetchLeaveTypes()
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    handleSubmit(values) {

        // show saving dialog
        this.setState({
            isOpen: true
        })

        const post = axios.post(leave_endpoint, values)

        post.then(({ data }) => {
            dispatch({ type: ActionTypes.RESET_LEAVE, payload: [] })
            Router.push(`/hrm/leaves/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({ isOpen: false })
        })

    }

    render() {

        const {
            employeesReducer,
            employmentTypesReducer,
            leaveTypesReducer,
            url
        } = this.props

        const _title = 'New Leave'
        const _menu = 'New Leave'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leaves', label: 'Leave' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="LeaveForm" />
                </div>
            </div>
        )

        let _render = null

        if (employeesReducer.fetched && leaveTypesReducer.fetched
        ) {
            const objToArr = Object.values(employeesReducer.employees);
            const convertEmpToArr = convertEmployeeObjectToArray(objToArr)

            _render = <LeaveForm
                id="newLveForm"
                onSubmit={this.handleSubmit}
                employees={convertEmpToArr}
                leaveTypes={Object.values(leaveTypesReducer.leavetypes)}
            />
        } else {
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        employeesReducer: state.employeesReducer,
        leaveTypesReducer: state.leaveTypesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployees: bindActionCreators(fnFetchEmployees, dispatch),
        onFetchLeaveTypes: bindActionCreators(fnFetchLeaveTypes, dispatch),
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveNew)