import React from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { DataGrid } from '../../../controls/input'
import { fnFetchLeaves } from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.props.onFetchLeaves()
    }

    handleDialogClose() {
        // fake receieve to we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_LEAVE, payload: [] })
    }

    render() {
        const { states, url } = this.props
        const _title = 'Leaves'
        const _menu = 'Leaves'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render employment type list
            let columns = [
                { text: 'Employee', name: 'employee', subname: 'name', link: true },
                { text: 'Leave Type', name: 'leaveType', subname: 'name' },
                { text: 'Start Date', name: 'fromDate', type: 'date' },
                { text: 'End Date', name: 'toDate', type: 'date' },
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.leaves} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.leavesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchLeaves: bindActionCreators(fnFetchLeaves, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveIndex)