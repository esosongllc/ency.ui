// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'
import moment from 'moment';

// ency packages
import Layout from '../../../components/Layout'
import { Page, Section, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton, NonEditableTextField, NonEditableTextArea } from '../../../controls/input'
import { GetById, fnFetchLeave } from '../../../controllers/functions'

const { dispatch } = makeStore()

class LeaveEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false,
            editPane: null,
            isOpen: false
        }
    }

    componentWillMount() {
        let { url, states } = this.props

        var leave = GetById(states.leaves, url.query.id)
        if (undefined === leave || null === leave) {
            this.props.onFetchLeave(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_LEAVE, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/leaves', label: 'Leave' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Leave'

        let _buttons = null


        if (!states.fetching && states.fetched && !states.error) {

            const leave = GetById(states.leaves, url.query.id)

            _title = `${leave.code} - ${_menu}`
            _menu = (<div>{leave.code}</div>)

            _buttons = (
                <div></div>
            )

            const fromDate = moment(leave.fromDate).format('DD-MMM-YYYY')
            const toDate = moment(leave.toDate).format('DD-MMM-YYYY')
            const postDate = moment(leave.submittedOn).format('DD-MMM-YYYY')

            _render = (
                <div>
                    <Section>
                        <div className="col50 padd-right-15">
                            <NonEditableTextField label="Employee" value={leave.employee.name} bold={true} />
                        </div>

                        <div className="col50 padd-left-15">
                            <NonEditableTextField label="Levae Type" value={leave.leaveType.name} bold={true} />
                            <NonEditableTextField label="Status" value={leave.status} />
                        </div>
                    </Section>

                    <Section>
                        <div className="col50 padd-right-15">
                            <NonEditableTextField label="Start Date" value={fromDate} bold={true} />
                            <NonEditableTextField label="End Date" value={toDate} bold={true} />
                            <NonEditableTextField label="Submitted On" value={postDate} />
                        </div>

                        <div className="col50 padd-left-15">
                            <NonEditableTextArea label="Reason" value={leave.reason}  />
                        </div>
                    </Section>
                </div>
            )

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.leavesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchLeave: bindActionCreators(fnFetchLeave, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(LeaveEdit)
