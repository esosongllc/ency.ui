// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { DataGrid } from '../../../controls/input'
import { employee_get } from '../../../controllers/apicalls'
import { fnFetchEmployees } from '../../../controllers/functions'

const { dispatch } = makeStore()

class EmployeeIndex extends React.Component {
    constructor(props) {
        super(props)
    }

    componentWillMount() {
        this.props.onFetchEmployees()
    }

    handleDialogClose() {
        // fake receieve to we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_EMPLOYEE, payload: [] })
    }

    render() {

        const { states, url } = this.props
        const _title = 'Emplolyees'
        const _menu = 'Employees'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched) {
            //render employee list
            const columns = [
                { text: 'Full Name', name: ['firstName', 'lastName'], link: true, merge: true },
                { text: 'Status', name: 'status' },
                { text: 'Designation', name: 'designation', subname:'name' },
                { text: '', name: 'key' }
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.employees} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error}
                        closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.employeesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployees: bindActionCreators(fnFetchEmployees, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(EmployeeIndex)
