
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import EmployeeForm from '../../../components/hrm/forms/employeeform'
import { employee_post } from '../../../controllers/apicalls'

import {
    fnFetchEmployees,
    fnFetchEmploymentTypes,
    fnFetchBranches,
    fnFetchDepartments,
    fnFetchDesignations,

    convertEmployeeObjectToArray
} from '../../../controllers/functions'

const { dispatch } = makeStore()

class EmployeeNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showPanel: false,
            isOpen: false
        }
        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    componentWillMount() {
        this.props.onFetchEmployees()
        this.props.onFetchEmploymentTypes()
        this.props.onFetchBranches()
        this.props.onFetchDepartments()
        this.props.onFetchDesignations()
    }


    componentDidMount() {
    }

    handleSubmit(values) {
        // show saving dialog
        this.setState({
            isOpen: true
        })
        
        
        let post = axios.post(employee_post, values)
        
        post.then(({ data }) => {
            dispatch({ type: ActionTypes.RESET_EMPLOYEE, payload: [] })
            Router.push(`/hrm/employees/edit?id=${data.id}`)
        }).catch(() => {
            this.setState({
                isOpen: false
            })
        })     
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    render() {

        const {
            employeesReducer,
            employmentTypesReducer,
            branchesReducer,
            deaprtmentsReducer,
            designationsReducer,
            url
        } = this.props

        const _title = 'New Emplolyee'
        const _menu = 'New Employee'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/employees', label: 'Employees' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={this.handleBack} /></div>
                <div>
                    <SubmitButton label="Save" form="EmployeeForm" />
                </div>
            </div>
        )

        let _render = null

        if (employeesReducer.fetched &&
            employmentTypesReducer.fetched &&
            branchesReducer.fetched &&
            deaprtmentsReducer.fetched &&
            designationsReducer.fetched
        ) {

            const objToArr = Object.values(employeesReducer.employees);
            const convertEmpToArr = convertEmployeeObjectToArray(objToArr)

            _render = <EmployeeForm
                id="newEmpFrm"
                onSubmit={this.handleSubmit}
                employees={convertEmpToArr}
                employmentTypes={Object.values(employmentTypesReducer.employmentTypes)}
                branches={Object.values(branchesReducer.branches)}
                departments={Object.values(deaprtmentsReducer.departments)}
                designations={Object.values(designationsReducer.designations)}
            />

        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }



        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        employeesReducer: state.employeesReducer,
        employmentTypesReducer: state.employmentTypesReducer,
        branchesReducer: state.branchesReducer,
        deaprtmentsReducer: state.deaprtmentsReducer,
        designationsReducer: state.designationsReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployees: bindActionCreators(fnFetchEmployees, dispatch),
        onFetchEmploymentTypes: bindActionCreators(fnFetchEmploymentTypes, dispatch),
        onFetchBranches: bindActionCreators(fnFetchBranches, dispatch),
        onFetchDepartments: bindActionCreators(fnFetchDepartments, dispatch),
        onFetchDesignations: bindActionCreators(fnFetchDesignations, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(EmployeeNew)

