// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import EmployeeView from '../../../components/hrm/views/employeeview'
import BasicInfo from '../../../components/hrm/employees/basicinfo'
import EmploymentDetails from '../../../components/hrm/employees/employmentdetails'
import { employee_get } from '../../../controllers/apicalls'
import { fnFetchEmployees, GetById } from '../../../controllers/functions'

const { dispatch } = makeStore()

class EmployeeEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            employee: null,
            showMenu: false,
            editPane: null,
            isOpen: false
        }
        this.showErrorDialog = this.showErrorDialog.bind(this)
        this.showEditPane = this.showEditPane.bind(this)
        this.toggleEditMenu = this.toggleEditMenu.bind(this)
        this.removeEditPane = this.removeEditPane.bind(this)
        this.onBasicInfoSubmit = this.onBasicInfoSubmit.bind(this)
    }

    showErrorDialog(state) {
        // should we show the error dialog
        this.setState({ isOpen: state })
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_RECEIVE_EMPLOYEE, payload: [] })
    }

    componentWillMount() {
        let { url } = this.props

        var employee = GetById(this.props.states.employees ,url.query.id)
        if (undefined === employee || null === employee) {
            this.props.onFetchEmployee(url.query.id)
        }
    }

    addEditPane(pane) {
        this.setState({ editPane: pane })
    }

    removeEditPane() {
        this.setState({ editPane: null })
    }

    toggleEditMenu() {
        this.setState({ showMenu: !this.state.showMenu })
    }

    onBasicInfoSubmit(values) {

        // // show saving dialog
        this.setState({
            isOpen: true
        })

        const put = axios.put(`${employee_get}/${values.id}/BasicInformation`, values)

        put.then(({ data }) => {

            dispatch({type:ActionTypes.EMPLOYEE_BASIC_INFORMATION, payload: values})

            this.removeEditPane()

            // hide saving dialog
            this.setState({
                isOpen: false
            })
        }).catch(() => {


            // hide saving dialog
            this.setState({
                isOpen: false
            })
        })
    }

    showEditPane({ item }) {
        const { employee, index } = item.props

        switch (index) {
            case 0:
                {
                    const basicinfo =
                        {
                            id: employee.id,
                            code: employee.code,
                            title: employee.title,
                            lastName: employee.lastName,
                            firstName: employee.firstName,
                            middleName: employee.middleName,
                            gender: employee.gender,
                            birthDate: employee.birthDate,
                            nationality: employee.nationality
                        }

                    this.addEditPane((<BasicInfo
                        employee={employee}
                        onSubmit={this.onBasicInfoSubmit}
                        initialValues={basicinfo}
                        onClose={this.removeEditPane} />))
                    break;
                }
            case 1: {
                this.addEditPane((<EmploymentDetails onClose={this.removeEditPane} />))
                break;
            }
        }

        this.toggleEditMenu()
    }


    render() {
        const { states, url } = this.props

        const breadcrumb = (
        <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/employees', label: 'Emloyees' }
            ]} />
        )
        let _title = null
        let _render = null
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Employee'
        let _buttons = null

        if (!states.fetching && states.fetched) {
            const employee = GetById(this.props.states.employees ,url.query.id)

            _title = `${employee.firstName} ${employee.lastName}`
            _menu = (<div>{_title}</div>)

            _buttons = (
                <div>
                    <div className="encyui-menu-rc">
                        <IconButton
                            label="Edit"
                            cancel={true}
                            iconClass="fa fa-caret-down"
                            cb={() => this.toggleEditMenu()} />

                        {/* Edit Menu Items  */}
                        <Menu
                            style={{ minWidth: '180px', top: '54px', left: '5px', display: display }}
                            onClick={this.showEditPane}>

                            <MenuItem key='0' employee={employee} >Basic Information</MenuItem>
                            <MenuItem key='1' employee={employee} >Emoloyment Details</MenuItem>
                            <MenuItem key='2' employee={employee} >Job Profile</MenuItem>
                            <MenuItem key='3' employee={employee} >Organization Profile</MenuItem>
                            <MenuItem key='4' employee={employee} >Contact Details</MenuItem>
                            <MenuItem key='5' employee={employee} >Educational Qualification</MenuItem>
                        </Menu>
                    </div>
                </div>
            )

            _render = <EmployeeView employee={employee} />
        }
        else {
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
                {this.state.editPane}
            </Layout>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        states: state.employeesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployee: bindActionCreators(fnFetchEmployees, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(EmployeeEdit)