
// npm packages
import React from 'react'
import Router from 'next/router'
import withRedux from 'next-redux-wrapper'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, SavingDialog } from '../../../controls/layout'
import { makeStore } from '../../../state/store'
import { ActionTypes } from '../../../state/actions'
import { Breadcrumb, Button, SubmitButton } from '../../../controls/input'
import BranchForm from '../../../components/hrm/forms/branchform'
import { branch_post } from '../../../controllers/apicalls'

const { dispatch } = makeStore()

class BranchNew extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }

        this.handleBack = this.handleBack.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    componentDidMount() {
        dispatch({ type: ActionTypes.RESET_BRANCH, payload: [] })
    }

    handleSubmit(values) {
        // show saving dialog
        this.setState({
            isOpen: true
        })


        const post = axios.post(branch_post, values)

        post.then(({ data }) => {
            Router.push(`/hrm/branches/edit?id=${data.id}`)
        }).catch((error) => {
            this.setState({ isOpen: false })
        })
    }

    handleBack() {
        this.setState({
            isOpen: false
        })
        history.back();
    }

    render() {
        const _title = 'New Branch'
        const _menu = 'New Branch'
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/branches', label: 'Branches' }
            ]} />
        )

        const _buttons = (
            <div>
                <div><Button label="Cancel" cancel={true} cb={() => history.back()} /></div>
                <div>
                    <SubmitButton label="Save" form="BranchForm" />
                </div>
            </div>
        )

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    <BranchForm id="newBrnForm" onSubmit={this.handleSubmit} />
                    <SavingDialog isOpen={this.state.isOpen} />
                </Page>
            </Layout>
        );
    }
}

export default withRedux(makeStore)(BranchNew);