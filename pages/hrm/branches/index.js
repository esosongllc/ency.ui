
import React, { Component } from 'react'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, LoadingDialog, FetchErrorDialog } from '../../../controls/layout'
import { Breadcrumb, NewButton } from '../../../controls/input'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { branch_get } from '../../../controllers/apicalls'
import { DataGrid } from '../../../controls/input'

const { dispatch } = makeStore()

class BranchIndex extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.onFetchBranches()
    }


    render() {

        const { states, url } = this.props
        const _title = 'Branches'
        const _menu = 'Branches'

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' }
            ]} />
        )

        let _render = null

        const _buttons = (
            <div>
                <NewButton page={url.pathname} />
            </div>
        )

        if (!states.fetching && states.fetched && !states.error) {
            //render employment type list
            let columns = [
                { text: 'Branch', name: 'name', link: true },
                { text: 'Location', name: 'location' },
                { text: '', name: 'key' }
            ]

            _render = <DataGrid pathname={url.pathname} columns={columns} data={states.branches} />
        } else {
            // render a loading panel
            _render = <LoadingDialog />
        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error}
                        closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const onFetchBranches = () => {
    // show loading dialog
    dispatch({ type: ActionTypes.FETCH_BRANCH, payload: [] })

    const request = axios.get(branch_get)

    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: ActionTypes.RECEIVE_BRANCH_LIST, payload: data })
        }).catch((error) => {
            dispatch({ type: ActionTypes.FETCH_BRANCH_ERROR, payload: error })
        })
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.branchesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchBranches: bindActionCreators(onFetchBranches, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(BranchIndex);