// npm packages
import React from 'react'
import Link from 'next/link'
import withRedux from 'next-redux-wrapper'
import { bindActionCreators } from 'redux'
import Menu, { SubMenu, Item as MenuItem, Divider } from 'rc-menu'
import axios from 'axios'

// ency packages
import Layout from '../../../components/Layout'
import { Page, FetchErrorDialog, LoadingDialog } from '../../../controls/layout'
import { ActionTypes } from '../../../state/actions'
import { makeStore } from '../../../state/store'
import { Breadcrumb, Button, SubmitButton, IconButton, NewButton } from '../../../controls/input'
import BranchView from '../../../components/hrm/views/branchview'
import { branch_get } from '../../../controllers/apicalls'
import { GetById, fnFetchBranch } from '../../../controllers/functions'

const { dispatch } = makeStore()

class BranchEdit extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showMenu: false
        }
    }

    componentWillMount() {
        let { url } = this.props

        var branch = GetById(this.props.states.branches, url.query.id)
        if (undefined === branch || null === branch) {
            this.props.onFetchBranch(url.query.id)
        }
    }

    handleDialogClose() {
        // fake receieve so we can remove the dialog box
        dispatch({ type: ActionTypes.RESET_RECEIVE_BRANCH, payload: [] })
    }


    render() {

        const { states, url } = this.props
        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/hrm', label: 'Human Resource' },
                { url: '/hrm/branches', label: 'Branches' }
            ]} />
        )
        let _title = 'Branch'
        let _render = 'Branch'
        let display = this.state.showMenu ? 'flex' : 'none'

        let _menu = 'Branch'

        let _buttons = null

        if (!states.fetching && states.fetched && !states.error) {

            const branch = GetById(states.branches, url.query.id)

            _title = `${branch.name} - ${_menu}`
            _menu = (<div>{branch.name}</div>)

            _buttons = (
                <div></div>
            )

            _render = <BranchView branch={branch} />

        } else {

            _render = <LoadingDialog />

        }

        return (
            <Layout title={_title} menu={_menu} breadcrumb={breadcrumb} controls={_buttons}>
                <Page>
                    {_render}

                    <FetchErrorDialog isOpen={states.error} closeCb={this.handleDialogClose} />
                </Page>
            </Layout>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        states: state.branchesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchBranch: bindActionCreators(fnFetchBranch, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(BranchEdit)