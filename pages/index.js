import * as React from 'react'
import { bindActionCreators } from 'redux'
import Link from 'next/link'

import Layout from '../components/Layout'
import TextBox from '../controls/input'
import { makeStore } from '../state/store'
import withRedux from 'next-redux-wrapper'
import { ActionTypes } from '../state/actions'
import { Page } from '../controls/layout'
import AppAside from '../controls/appaside'
import { HeadingIcons } from '../controls/HeadingLinks'
import { Breadcrumb } from '../controls/input'


class Index extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {

    }

    render() {

        const breadcrumb = (
            <Breadcrumb links={[
                { url: '/', label: 'Dashboard' }
            ]} />
        )

        return (
            <Layout title="Dashboard" menu="Dashboard" breadcrumb={breadcrumb}>
                <Page aside={true} asideContent={AppAside} >
                </Page>
            </Layout>)
    }
}

const cbFetchEmployees = () => {
    return { type: ActionTypes.FETCH_EMPLOYEES, payload: {} }
}

const mapStateToProps = (state) => {
    return {
        employees: state.employeesReducer
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onFetchEmployees: bindActionCreators(cbFetchEmployees, dispatch)
    }
}

export default withRedux(makeStore, mapStateToProps, mapDispatchToProps)(Index)