

const express = require('express')
const next = require('next')
const morgan = require('morgan')

const dev = process.env.NODE_ENV != 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


app.
    prepare().
    then(() => {
        const server = express()

        // server.use(morgan('combined'))

        // server.get('/hrm/employees/:id', (req, res) => {
        //     const actualPage = '/hrm/employees/edit'
        //     const queryParams = req.params.id.match(/edit/i) ? { id: req.query.id }:{ id: req.params.id }
        //     app.render(req, res, actualPage, queryParams)
        // })

        server.get('*', (req, res) => {
            return handle(req, res)
        })

        server.listen(3000, (err) => {
            if (err) {
                throw err
            }
            console.log('> Ready on http://localhost:3000')
        })
    }).
    catch((e) => {
        console.error(e.stack)
        process.exit(1)
    })