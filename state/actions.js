
export const ActionTypes = {
    REDUX_INIT: '@@redux/INIT',
    REDUXFORM_REGISTER_FIELD: '@@redux-form/REGISTER_FIELD',
    REDUXFORM_FOCUS: '@@redux-form/FOCUS',
    REDUXFORM_SUBMIT_SUCCEEDED: '@@redux-form/SET_SUBMIT_SUCCEEDED',
    REDUXFORM_DESTROY: '@@redux-form/DESTROY',

    // Employees
    RESET_EMPLOYEE: 'RESET_EMPLOYEE',
    FETCH_EMPLOYEE: 'FETCH_EMPLOYEE',
    RECEIVE_EMPLOYEE_SINGLE: 'RECEIVE_EMPLOYEE_SINGLE',
    RECEIVE_EMPLOYEE_LIST: 'RECEIVE_EMPLOYEE_LIST',
    EMPLOYEE_BASIC_INFORMATION: 'EMPLOYEE_BASIC_INFORMATION',
    FETCH_EMPLOYEE_ERROR: 'FETCH_EMPLOYEE_ERROR',

    // Employment Types
    RESET_EMPLOYMENTTYPE: 'RESET_EMPLOYMENTTYPE',
    FETCH_EMPLOYMENTTYPE: 'FETCH_EMPLOYMENTTYPE',
    RECEIVE_EMPLOYMENTTYPE_SINGLE: 'RECEIVE_EMPLOYMENTTYPE_SINGLE',
    RECEIVE_EMPLOYMENTTYPE_LIST: 'RECEIVE_EMPLOYMENTTYPE_LIST',
    FETCH_EMPLOYMENTTYPE_ERROR: 'FETCH_EMPLOYMENTTYPE_ERROR',

    // Branches
    RESET_BRANCH: 'RESET_BRANCH',
    FETCH_BRANCH: 'FETCH_BRANCH',
    RECEIVE_BRANCH_SINGLE: 'RECEIVE_BRANCH_SINGLE',
    RECEIVE_BRANCH_LIST: 'RECEIVE_BRANCH_LIST',
    FETCH_BRANCH_ERROR: 'FETCH_BRANCH_ERROR',

    // Departments
    RESET_DEPARTMENT: 'RESET_DEPARTMENT',
    FETCH_DEPARTMENT: 'FETCH_DEPARTMENT',
    RECEIVE_DEPARTMENT_SINGLE: 'RECEIVE_DEPARTMENT_SINGLE',
    RECEIVE_DEPARTMENT_LIST: 'RECEIVE_DEPARTMENT_LIST',
    FETCH_DEPARTMENT_ERROR:'FETCH_DEPARTMENT_ERROR',

    // Designations
    RESET_DESIGNATION:'RESET_DESIGNATION',
    FETCH_DESIGNATION:'FETCH_DESIGNATION',
    RECEIVE_DESIGNATION_SINGLE:'RECEIVE_DESIGNATION_SINGLE',
    RECEIVE_DESIGNATION_LIST:'RECEIVE_DESIGNATION_LIST',
    FETCH_DESIGNATION_ERROR:'FETCH_DESIGNATION_ERROR',

    // Leaves
    RESET_LEAVE:'RESET_LEAVE',
    FETCH_LEAVE:'FETCH_LEAVE',
    RECEIVE_LEAVE_SINGLE:'RECEIVE_LEAVE_SINGLE',
    RECEIVE_LEAVE_LIST:'RECEIVE_LEAVE_LIST',
    FETCH_LEAVE_ERROR:'FETCH_LEAVE_ERROR',    

    // Leave Types
    RESET_LEAVETYPE:'RESET_LEAVETYPE',
    FETCH_LEAVETYPE:'FETCH_LEAVETYPE',
    RECEIVE_LEAVETYPE_SINGLE:'RECEIVE_LEAVETYPE_SINGLE',
    RECEIVE_LEAVETYPE_LIST:'RECEIVE_LEAVETYPE_LIST',
    FETCH_LEAVETYPE_ERROR:'FETCH_LEAVETYPE_ERROR',

    // Leave Controls
    RESET_LEAVECONTROL:'RESET_LEAVECONTROL',
    FETCH_LEAVECONTROL:'FETCH_LEAVECONTROL',
    RECEIVE_LEAVECONTROL_SINGLE:'RECEIVE_LEAVECONTROL_SINGLE',
    RECEIVE_LEAVECONTROL_LIST:'RECEIVE_LEAVECONTROL_LIST',
    FETCH_LEAVECONTROL_ERROR:'FETCH_LEAVECONTROL_ERROR',
}

export const ActionControllers = {
}