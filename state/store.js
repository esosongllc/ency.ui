import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { reducer as formReducer } from 'redux-form'
import { composeWithDevTools } from 'redux-devtools-extension';
import InitialState from './state'
import { ActionTypes } from './actions'


const employeesReducer = (state = InitialState.EmployeesState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_EMPLOYEE:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_EMPLOYEE:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_EMPLOYEE_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    employees: { ...state.employees, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_EMPLOYEE_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, employees: action.payload }
            }
        case ActionTypes.EMPLOYEE_BASIC_INFORMATION:
            {
                var _new = action.payload
                var _old = state.employees[_new.id]
                var dict = {}

                dict[_new.id] = { ..._old, ..._new }

                return { ...state, employees: { ...state.employees, ...dict } }
            }
        case ActionTypes.FETCH_EMPLOYEE_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const employmentTypesReducer = (state = InitialState.EmploymentTypesState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_EMPLOYMENTTYPE:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_EMPLOYMENTTYPE:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_EMPLOYMENTTYPE_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    employmentTypes: { ...state.employmentTypes, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_EMPLOYMENTTYPE_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, employmentTypes: action.payload }
            }
        case ActionTypes.FETCH_EMPLOYMENTTYPE_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const branchesReducer = (state = InitialState.BranchesState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_BRANCH:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_BRANCH:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_BRANCH_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    branches: { ...state.branches, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_BRANCH_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, branches: action.payload }
            }
        case ActionTypes.FETCH_BRANCH_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const deaprtmentsReducer = (state = InitialState.DepartmentsState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_DEPARTMENT:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_DEPARTMENT:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_DEPARTMENT_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    departments: { ...state.departments, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_DEPARTMENT_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, departments: action.payload }
            }
        case ActionTypes.FETCH_DEPARTMENT_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const designationsReducer = (state = InitialState.DesignationsState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_DESIGNATION:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_DESIGNATION:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_DESIGNATION_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    designations: { ...state.designations, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_DESIGNATION_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, designations: action.payload }
            }
        case ActionTypes.FETCH_DESIGNATION_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const leavesReducer = (state = InitialState.LeavesState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_LEAVE:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_LEAVE:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_LEAVE_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    leaves: { ...state.leaves, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_LEAVE_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, leaves: action.payload }
            }
        case ActionTypes.FETCH_LEAVE_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const leaveTypesReducer = (state = InitialState.LeaveTypesState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_LEAVETYPE:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_LEAVETYPE:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_LEAVETYPE_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    leavetypes: { ...state.leavetypes, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_LEAVETYPE_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, leavetypes: action.payload }
            }
        case ActionTypes.FETCH_LEAVETYPE_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const leaveControlsReducer = (state = InitialState.LeaveControlsState, action) => {
    switch (action.type) {
        case ActionTypes.RESET_LEAVECONTROL:
            {
                return { ...state, fetching: false, fetched: false, error: false }
            }
        case ActionTypes.FETCH_LEAVECONTROL:
            {
                return { ...state, fetching: true, fetched: false, error: false }
            }
        case ActionTypes.RECEIVE_LEAVECONTROL_SINGLE:
            {
                return {
                    ...state, fetching: false, fetched: true, error: false,
                    leavecontrols: { ...state.leavecontrols, ...action.payload }
                }
            }
        case ActionTypes.RECEIVE_LEAVECONTROL_LIST:
            {
                return { ...state, fetching: false, fetched: true, error: false, leavecontrols: action.payload }
            }
        case ActionTypes.FETCH_LEAVECONTROL_ERROR:
            {
                return { ...state, fetching: false, fetched: false, error: true }
            }
    }
    return state
}

const rootReducer = combineReducers({
    form: formReducer,
    employeesReducer,
    employmentTypesReducer,
    branchesReducer,
    deaprtmentsReducer,
    designationsReducer,
    leavesReducer,
    leaveTypesReducer,
    leaveControlsReducer
})

// remove initial state from createStore as it was causing errors
// TODO 1000: Create Store
export const store = createStore(rootReducer, {}, composeWithDevTools(applyMiddleware(thunk)))

// this is for next.js
export const makeStore = () => store

