
export default {
    EmployeesState: {
        fetched: false,
        fetching: false,
        employees: {},
        errors: null,
        error: false
    },
    
    
    EmploymentTypesState: {
        fetched: false,
        fetching: false,
        employmentTypes: {},
        errors: null,
        error: false
    },


    BranchesState:{
        fetched: false,
        fetching: false,
        branches: {},
        errors: null,
        error: false
    },

    DepartmentsState:{
        fetched: false,
        fetching: false,
        departments: {},
        errors: null,
        error: false
    },

    DesignationsState:{
        fetched: false,
        fetching: false,
        designations: {},
        errors: null,
        error: false
    },

    LeavesState:{
        fetched: false,
        fetching: false,
        leaves: {},
        errors: null,
        error: false
    },

    LeaveTypesState:{
        fetched: false,
        fetching: false,
        leavetypes: {},
        errors: null,
        error: false
    },

    LeaveControlsState:{
        fetched: false,
        fetching: false,
        leavecontrols: {},
        errors: null,
        error: false
    }
}