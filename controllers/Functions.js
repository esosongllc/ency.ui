import uuid from 'uuid/v4'
import { ActionTypes } from '../state/actions'
import { makeStore } from '../state/store'
import axios from 'axios'
import * as EndPoints from './apicalls'

const { dispatch } = makeStore()

export const GetById = (values, id) => {
    var objectToArray = Object.values(values)
    return objectToArray.find((e) => e.id === id)
}

export const convertEmployeeObjectToArray = (employees) => {
    let _employees = []
    const emps = Object.values(employees)

    emps.forEach((e)=>{
        const name = `${e.lastName} ${e.firstName}`

        // we just only need the id and the name
        _employees.push({id: e.id, name: name})
    })

    return _employees
}

export const fnFetchEmployees = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_EMPLOYEE, payload: [] })

    const request = axios.get(EndPoints.employee_get)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_EMPLOYEE_LIST, payload: data })
        }).catch((error) => {
            dispatch({ type: ActionTypes.FETCH_EMPLOYEE_ERROR, payload: error })
        })
    }
}

export const fnFetchEmployee = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_EMPLOYEE, payload: [] })

    const request = axios.get(`${EndPoints.employee_get}/${id}`)
    
    return (dispatch) => {
        request.then(({ data }) => {

            dispatch({ type: ActionTypes.RECEIVE_EMPLOYEE_SINGLE, payload: data })

        }).catch((error) => { 
            
            dispatch({ type: ActionTypes.FETCH_EMPLOYEE_ERROR, payload: error })
        })
    }
}

export const fnFetchEmploymentType = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_EMPLOYMENTTYPE, payload: [] })

    const request = axios.get(`${EndPoints.employmenttype_get}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_EMPLOYMENTTYPE_SINGLE, payload: data })
        }).catch((error) => {
            // when it error
            dispatch({ type: ActionTypes.FETCH_EMPLOYMENTTYPE_ERROR, payload: error })
        })
    }
}

export const fnFetchEmploymentTypes = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_EMPLOYMENTTYPE, payload: [] })

    const request = axios.get(EndPoints.employmenttype_get)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_EMPLOYMENTTYPE_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_EMPLOYMENTTYPE_ERROR, payload: error })
        })
    }
}

export const fnFetchBranch = (id) => {
    dispatch({ type: ActionTypes.FETCH_BRANCH, payload: [] })

    const request = axios.get(`${EndPoints.branch_get}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: ActionTypes.RECEIVE_BRANCH_SINGLE, payload: data })
        }).catch((error) => {
            dispatch({ type: ActionTypes.FETCH_BRANCH_ERROR, payload: [] })
        })
    }
}

export  const fnFetchBranches = () => {
    // show loading dialog
    dispatch({ type: ActionTypes.FETCH_BRANCH, payload: [] })

    const request = axios.get(EndPoints.branch_get)

    return (dispatch) => {
        request.then(({ data }) => {
            dispatch({ type: ActionTypes.RECEIVE_BRANCH_LIST, payload: data })
        }).catch((error) => {
            dispatch({ type: ActionTypes.FETCH_BRANCH_ERROR, payload: error })
        })
    }
}

export const fnFetchDepartment = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_DEPARTMENT, payload: [] })

    const request = axios.get(`${EndPoints.department_get}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_DEPARTMENT_SINGLE, payload: data })
        }).catch((error) => {
            // when it error
            dispatch({ type: ActionTypes.FETCH_DEPARTMENT_ERROR, payload: error })
        })
    }
}

export const fnFetchDepartments = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_DEPARTMENT, payload: [] })

    const request = axios.get(EndPoints.department_get)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_DEPARTMENT_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_DEPARTMENT_ERROR, payload: error })
        })
    }
}

export const fnFetchDesignation = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_DESIGNATION, payload: [] })

    const request = axios.get(`${EndPoints.designation_get}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_DESIGNATION_SINGLE, payload: data })
        }).catch((error) => {
            // when it error
            dispatch({ type: ActionTypes.FETCH_DESIGNATION_ERROR, payload: error })
        })
    }
}

export const fnFetchDesignations = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_DESIGNATION, payload: [] })

    const request = axios.get(EndPoints.designation_get)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_DESIGNATION_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_DESIGNATION_ERROR, payload: error })
        })
    }
}

export const fnFetchLeaveControl = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVECONTROL, payload: [] })

    const request = axios.get(`${EndPoints.leavecontrol_endpoint}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVECONTROL_SINGLE, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVECONTROL_ERROR, payload: error })
        })
    }
}

export const fnFetchLeaveControls = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVECONTROL, payload: [] })

    const request = axios.get(EndPoints.leavecontrol_endpoint)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVECONTROL_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVECONTROL_ERROR, payload: error })
        })
    }
}

export const fnFetchLeaveType = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVETYPE, payload: [] })

    const request = axios.get(`${EndPoints.leavetype_endpoint}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVETYPE_SINGLE, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVETYPE_ERROR, payload: error })
        })
    }
}

export const fnFetchLeaveTypes = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVETYPE, payload: [] })

    const request = axios.get(EndPoints.leavetype_endpoint)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVETYPE_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVETYPE_ERROR, payload: error })
        })
    }
}

export const fnFetchLeave = (id) => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVE, payload: [] })

    const request = axios.get(`${EndPoints.leave_endpoint}/${id}`)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVE_SINGLE, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVE_ERROR, payload: error })
        })
    }
}

export const fnFetchLeaves = () => {
    // show loading 
    dispatch({ type: ActionTypes.FETCH_LEAVE, payload: [] })

    const request = axios.get(EndPoints.leave_endpoint)

    return (dispatch) => {
        request.then(({ data }) => {
            // we got the data
            dispatch({ type: ActionTypes.RECEIVE_LEAVE_LIST, payload: data })
        }).catch((error) => {
             // when it error
            dispatch({ type: ActionTypes.FETCH_LEAVE_ERROR, payload: error })
        })
    }
}
