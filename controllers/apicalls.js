
export const host = 'https://localhost:44310'

export const employee_get= `${host}/api/employees`
export const employee_post=`${host}/api/employees`

export const employmenttype_get = `${host}/api/employmenttypes`
export const employmenttype_post = `${host}/api/employmenttypes`

export const branch_get = `${host}/api/branches`
export const branch_post = `${host}/api/branches`

export const department_get = `${host}/api/departments`
export const department_post = `${host}/api/departments`

export const designation_get = `${host}/api/designations`
export const designation_post = `${host}/api/designations`

export const leave_endpoint = `${host}/api/leaves`

export const leavecontrol_endpoint = `${host}/api/leavecontrols`

export const leavetype_endpoint = `${host}/api/leavetypes`